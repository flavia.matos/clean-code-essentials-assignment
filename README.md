# Clean Code Essentials

## Separation of Concerns

MVC

## Design Patterns

Adapter: new posts can come with two different params (`text` and `tweet`). The adapter will act as a translator converting tweets to texts, building a valid Post.

## Design principles

DRY: `getPosts`, `getText` and `validatePost` are examples where I created a new function to handle logic that is required in more than one place, avoiding repetition.

YAGNI: for now, the application only creates, edits and lists posts. A future feature can be deleting a post, for example.

KISS: the functions are simple with descriptive names.
