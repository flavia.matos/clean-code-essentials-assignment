const express = require('express');
const PostController = require('../controllers/post');

const router = express.Router();

router.get('/list', async (req, res) => {
    PostController.listPosts(req, res);
});

router.post('/add', async (req, res) => {
    PostController.createPost(req, res);
});

router.patch('/edit/:postId', async (req, res) => {
    PostController.editPost(req, res);
});

router.get('/', async (req, res) => {
    const posts = await PostController.getPosts();
    res.render('main', { layout : 'index', posts });
});

module.exports = router;
