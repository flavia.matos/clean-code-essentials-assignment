const validatePost = (body) => {
    if (!body) return false;
    if (isString(body.text) || isString(body.tweet)) {
        return true;
    }
}

const getText = (body) => body.text || body.tweet;

const isString = (value) => typeof value === "string";

module.exports = { validatePost, getText, isString };
