const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const handlebars = require('express-handlebars');
const postsRoute = require('./routes/posts');
require('dotenv/config');

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use('/posts', postsRoute);
app.set('view engine', 'hbs');
app.engine('hbs', handlebars({
    extname: 'hbs'
}));

mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true, useUnifiedTopology: true },
    () => console.log('connected')
);

app.listen(port);
