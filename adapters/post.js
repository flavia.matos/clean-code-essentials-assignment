const Post = require('../models/Post');
const { getText } = require('../utils');

module.exports = (params) => {
    const text = getText(params);
    if (text) {
        return new Post({ text });
    }
}
