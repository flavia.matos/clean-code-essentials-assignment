const postAdapter = require('../adapters/post');
const Post = require('../models/Post');
const { getText, validatePost } = require('../utils');

const createPost = async (req, res) => {
    const isValidPost = validatePost(req.body);
    if (!isValidPost) {
        return res.status(400).json({ error: "Invalid post" });
    }
    const post = postAdapter(req.body);
    try {
        const savedPost = await post.save();
        return res.status(200).json(savedPost);
    } catch (err) {
        return res.status(400).json({ error: err });
    }
}

const editPost = async (req, res) => {
    console.log(req.body);
    const isValidPost = validatePost(req.body);
    if (!isValidPost) {
        return res.status(400).json({ error: "Invalid post" });
    }
    try {
        const { postId:_id } = req.params;
        const text = getText(req.body);
        const editedPost = await Post.updateOne({ _id }, { $set: { text } });
        return res.status(200).json(editedPost);
    } catch (err) {
        return res.status(400).json({ error: err });
    }
}

const listPosts = async (req, res) => {
    try {
        const posts = await getPosts();
        return res.status(200).json(posts);
    } catch(err) {
        return res.status(400).json({ error: err });
    }
}

const getPosts = async () => {
    return await Post.find().lean();
}

module.exports = { createPost, editPost, listPosts, getPosts };
