const { Schema, model } = require('mongoose');

const PostSchema = Schema({
    text: String,
});

module.exports = model('Posts', PostSchema);
